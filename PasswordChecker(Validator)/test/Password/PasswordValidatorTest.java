package Password;

import static org.junit.Assert.*;

/**
 * 
 * @author Harman Bath 991540198
 * 
 */

import org.junit.Test;

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid characters", PasswordValidator.hasValidCaseChars("asdajDDasd"));
	}
	
	@Test
	public void testHasValidCaseCharsExpcetion() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));	
	}
	
	@Test
	public void testHasValidCaseCharsExpcetionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));	
	}
	
	@Test
	public void testHasValidCaseCharsExpcetionBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));	
	}
	
	@Test
	public void testHasValidCaseCharsExpcetionBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));	
	}
	
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid characters", PasswordValidator.hasValidCaseChars("aA"));
	}
	
	@Test
	public void testContainsTwoDigitsRegular() {
		boolean passLength = PasswordValidator.containsTwoDigits("12"); 
		assertTrue("Password needs two digits", passLength);
	}
	
	@Test
	public void testContainsTwoDigitsException() {
		boolean passLength = PasswordValidator.containsTwoDigits("HelloWorld"); 
		assertFalse("Password needs two digits", passLength);
	}
	
	@Test
	public void testContainsTwoDigitsBoundaryIn() {
		boolean passLength = PasswordValidator.containsTwoDigits("41"); 
		assertTrue("Password needs two digits", passLength);
	}
	
	@Test
	public void testContainsTwoDigitsBoundaryOut() {
		boolean passLength = PasswordValidator.containsTwoDigits("4"); 
		assertFalse("Password needs two digits", passLength);
	}

	
	@Test
	public void testIsValidLengthRegular() {
		boolean passLength = PasswordValidator.isValidLength("HelloWorld"); 
		assertTrue("Invalid password length", passLength);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean passLength = PasswordValidator.isValidLength("Hello");
		assertFalse("Invalid password length", passLength); 
	}
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean passLength = PasswordValidator.isValidLength("Hello World");
		assertFalse("Invalid password length", passLength);
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean passLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid password length", passLength);	
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean passLength = PasswordValidator.isValidLength("HelloHi");
		assertFalse("Invalid password length", passLength);
	}
	
	@Test
	public void testIsValidBoundaryIn() {
		boolean passLength = PasswordValidator.isValidLength("Hello123");
		assertTrue("Invalid password length", passLength); 
	}
}
