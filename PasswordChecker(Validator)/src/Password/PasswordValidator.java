package Password;

/**
 * 
 * @author Harman Bath 991540198
 * 
 */

public class PasswordValidator {
	
	
	private static int MIN_LENGTH = 8; 
	static boolean correct = true; 
	
	
	public static boolean hasValidCaseChars(String password) {
		return password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	public static boolean isValidLength(String password) {
		
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH); 
	}
	
	public static boolean containsTwoDigits(String password) {
		
		int digit = 0; 
		char element; 
		
		for(int index = 0; index < password.length(); index++) {
			element = password.charAt(index);
			if(Character.isDigit(element)) {
				digit++;
			}
		}
		
		if(digit < 2) {
			return false;  
		}
		
		return correct; 
	}
	
}
